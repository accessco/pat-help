import asyncio
import websockets

async def main():
    async with websockets.connect("ws://localhost:1616") as websocketModule:
        # Security Check
        serverResponse = await websocketModule.recv()       
        await websocketModule.send('Help')
        await websocketModule.recv()  
        command = await websocketModule.recv()

        # Functionality 


        await websocketModule.send('terminate')
        exit()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close


